package com.sr.mobiquity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.Menu
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.sr.mobiquity.databinding.ActivityMainBinding
import com.sr.mobiquity.fragments.CityFragment
import com.sr.mobiquity.fragments.HelpFragment
import com.sr.mobiquity.fragments.HomeFragment
import com.sr.mobiquity.fragments.SettingsFragment
import com.sr.mobiquity.models.City

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        addFragment(HomeFragment.newInstance(object : ActionsListener {
            override fun onCitySelected(city: City) {
                addFragment(CityFragment.newInstance(city), "City Fragment", true)
            }

            override fun onSettingsSelected() {
                addFragment(SettingsFragment(), "Settings Fragment", true)
            }

            override fun onHelpSelected() {
                addFragment(HelpFragment(), "Help Fragment", true)
            }
        }), "Home Fragment", false)
    }

    private fun addFragment(fragment: Fragment, tag: String, addToBackStack: Boolean) {
        if (addToBackStack) {
            supportFragmentManager.beginTransaction().add(R.id.fragmentContainer, fragment, tag)
                .addToBackStack(tag).commit()
        } else {
            supportFragmentManager.beginTransaction().add(R.id.fragmentContainer, fragment, tag)
                .commit()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && supportFragmentManager.fragments.size > 0) {
            supportActionBar?.title = getString(R.string.app_name)
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return true
    }

    fun removeAll() {
        if (supportFragmentManager.fragments.size > 0)
            (supportFragmentManager.fragments[0] as HomeFragment).removeAll()
    }

    interface ActionsListener {
        fun onCitySelected(city: City)
        fun onSettingsSelected()
        fun onHelpSelected()
    }
}