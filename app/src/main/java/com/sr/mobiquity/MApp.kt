package com.sr.mobiquity

import android.app.Application
import androidx.room.Room
import com.google.gson.GsonBuilder
import com.sr.mobiquity.database.AppDatabase
import com.sr.mobiquity.database.CityDao
import com.sr.mobiquity.pref.SystemPref
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MApp : Application() {
    private val BASE_URL = "http://api.openweathermap.org/data/2.5/"

    companion object {
        private lateinit var mInstance: MApp
        private lateinit var retrofit: Retrofit
        private lateinit var database: AppDatabase
        private lateinit var mPref: SystemPref

        @JvmStatic
        fun getAPIService(): APIServices {
            return retrofit.create(APIServices::class.java)
        }

        @JvmStatic
        fun getCityDao(): CityDao {
            return database.cityDao()
        }

        @JvmStatic
        fun getPref(): SystemPref {
            return mPref
        }

        fun getInstance(): MApp {
            return mInstance
        }
    }

    override fun onCreate() {
        super.onCreate()

        mInstance = this;
        initRetrofit()
        initDatabase()
        initPref()
    }

    private fun initPref() {
        mPref = SystemPref().init(SystemPref.PREF_NAME)
    }

    private fun initDatabase() {
        database =
            Room.databaseBuilder(applicationContext, AppDatabase::class.java, "M-db")
                .build()
    }

    private fun initRetrofit() {
        val gson = GsonBuilder().setLenient().create()

        retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
}