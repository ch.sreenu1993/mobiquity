package com.sr.mobiquity.adapters

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sr.mobiquity.fragments.HomeFragment
import com.sr.mobiquity.MApp
import com.sr.mobiquity.MainActivity
import com.sr.mobiquity.R
import com.sr.mobiquity.adapters.CitiesAdapter.CityViewHolder
import com.sr.mobiquity.models.City
import java.util.concurrent.Executors

class CitiesAdapter : RecyclerView.Adapter<CityViewHolder>() {
    private lateinit var adapterListener: HomeFragment.AdapterListener
    private lateinit var listener: MainActivity.ActionsListener
    private var cities: MutableList<City> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        return CityViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_cities_recycler_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        val city = cities[position]
        holder.name.text = city.name
        holder.address.text = city.address
        holder.latlng.text = "${city.latitude} & ${city.longitude}"
    }

    override fun getItemCount(): Int {
        return cities.size
    }

    fun addAll(cities: List<City>) {
        this.cities = cities as MutableList<City>
        notifyDataSetChanged()
    }

    fun addCity(city: City) {
        cities.add(city)
        notifyDataSetChanged()
    }

    fun setListeners(
        listener: MainActivity.ActionsListener,
        adapterListener: HomeFragment.AdapterListener
    ) {
        this.listener = listener
        this.adapterListener = adapterListener
    }

    fun deleteAll() {
        cities.clear()
        notifyDataSetChanged()
    }

    inner class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.nameTextView)
        var address: TextView = itemView.findViewById(R.id.addressTextView)
        var delete: ImageView = itemView.findViewById(R.id.deleteImageView)
        var latlng: TextView = itemView.findViewById(R.id.latlngTextView)

        init {
            itemView.setOnClickListener{
                listener.onCitySelected(cities.get(adapterPosition))
            }

            delete.setOnClickListener {
                adapterListener.onCityDeleted(cities.get(adapterPosition),adapterPosition)
                Executors.newSingleThreadExecutor().execute(Runnable {
                    MApp.getCityDao().delete(cities.get(adapterPosition))
                    cities.removeAt(adapterPosition)
                    Handler(Looper.getMainLooper()).post(Runnable {
                        notifyDataSetChanged()
                    })
                })
            }
        }
    }
}