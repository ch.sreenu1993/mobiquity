package com.sr.mobiquity.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sr.mobiquity.MApp
import com.sr.mobiquity.R
import com.sr.mobiquity.adapters.ForecastAdapter.ForecastViewHolder
import com.sr.pojos.WeatherInfo
import java.util.*

class ForecastAdapter(infoList: ArrayList<WeatherInfo>) :
    RecyclerView.Adapter<ForecastViewHolder>() {
    private var infos = ArrayList<WeatherInfo>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        return ForecastViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_forcast_recycler_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        holder.dateTextView.text = infos[position].dtTxt!!.split(" ")[0]
        holder.dateTextView.visibility = View.VISIBLE;
        holder.temperatureTextView.text = "${infos[position].dtTxt!!.split(" ")[1]}  " +
                " ${holder.itemView.context.getString(
                    R.string.set_temperature,
                    "${infos[position].main?.temp}", MApp.getPref().getTempUnit()
                )}"
        holder.descriptionTextView.text = infos[position].weather?.get(0)?.description
        if (position == 0) {
            holder.dateTextView.visibility = View.VISIBLE
        } else {
            if (infos[position].dtTxt!!.split(" ")[0].split("-")[2] ==
                infos[position - 1].dtTxt!!.split(" ")[0].split("-")[2]
            ) {
                holder.dateTextView.visibility = View.GONE;
            } else {
                holder.dateTextView.visibility = View.VISIBLE;
            }
        }

        if (position < infos.size - 1) {
            if (infos[position + 1].dtTxt!!.split(" ")[0].split("-")[2] ==
                infos[position].dtTxt!!.split(" ")[0].split("-")[2]
            ) {
                holder.view.visibility = View.GONE
            } else {
                holder.view.visibility = View.VISIBLE
            }
        } else {
            holder.view.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return infos.size
    }

    class ForecastViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val dateTextView: TextView = itemView.findViewById(R.id.date_text_view)
        val temperatureTextView: TextView =
            itemView.findViewById(R.id.temperature_text_view)
        val descriptionTextView: TextView =
            itemView.findViewById(R.id.description_text_view)
        val view: View = itemView.findViewById(R.id.forecast_view)

    }

    init {
        infos = infoList
    }
}