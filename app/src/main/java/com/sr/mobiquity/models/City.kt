package com.sr.mobiquity.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class City (
    @ColumnInfo val name: String?,
    @ColumnInfo var address: String?,
    @ColumnInfo var latitude :Double,
    @ColumnInfo var longitude: Double
){
    @PrimaryKey(autoGenerate = true) var id: Int = 0
}
