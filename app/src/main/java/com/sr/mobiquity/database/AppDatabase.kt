package com.sr.mobiquity.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sr.mobiquity.models.City

@Database(entities = arrayOf(City::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cityDao(): CityDao
}