package com.sr.mobiquity.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.sr.mobiquity.models.City

@Dao
interface CityDao {
    @Insert
    fun insert(vararg city: City)

    @Query("SELECT * FROM City WHERE id = :id")
    fun getCity(id: Int): City

    @Query("SELECT * FROM city")
    fun getAll(): List<City>

    @Delete
    fun delete(city: City)

    @Query("DELETE FROM City")
    fun deleteAll()
}