package com.sr.mobiquity.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.sr.mobiquity.R

class HelpFragment : Fragment() {
   private lateinit var webView : WebView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_help, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        webView = view.findViewById(R.id.help_web_view)
        webView.loadUrl("file:///android_asset/index.html");
    }

    override fun onResume() {
        super.onResume()

        (activity as AppCompatActivity).supportActionBar?.title = "Help"
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.settings).isVisible = false;
        menu.findItem(R.id.help).isVisible = false;
        super.onPrepareOptionsMenu(menu)
    }
}