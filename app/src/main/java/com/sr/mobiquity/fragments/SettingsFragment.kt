package com.sr.mobiquity.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.sr.mobiquity.MApp
import com.sr.mobiquity.MainActivity
import com.sr.mobiquity.R
import com.sr.mobiquity.pref.SystemPref
import java.util.concurrent.Executors

class SettingsFragment : Fragment() {
    lateinit var unitsRadioGroup: RadioGroup
    lateinit var forecastRadioGroup: RadioGroup
    lateinit var cityLimitEditText: EditText
    lateinit var resetBookmarksButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        unitsRadioGroup = view.findViewById(R.id.settings_units_radio_group)
        forecastRadioGroup = view.findViewById(R.id.settings_forcast_radio_group)
        cityLimitEditText = view.findViewById(R.id.settings_no_of_cities_edit_text)
        resetBookmarksButton = view.findViewById(R.id.reset_bookmarked_cities_button)

        when (MApp.getPref()[SystemPref.PrefKeys.units, "standard"]) {
            "standard" -> {
                unitsRadioGroup.check(R.id.radio_button_1)
            }
            "metric" -> {
                unitsRadioGroup.check(R.id.radio_button_2)
            }
            "imperial" -> {
                unitsRadioGroup.check(R.id.radio_button_3)
            }
        }

        when (MApp.getPref()[SystemPref.PrefKeys.forecast, "No"]) {
            "No" -> {
                forecastRadioGroup.check(R.id.radio_button_4)
            }
            "5 Days" -> {
                forecastRadioGroup.check(R.id.radio_button_5)
            }
        }

        cityLimitEditText.setText("${MApp.getPref()[SystemPref.PrefKeys.citiesLimit, 10]}")

        resetBookmarksButton.setOnClickListener {
            androidx.appcompat.app.AlertDialog.Builder(view.context)
                .setTitle("Reset")
                .setMessage("Are you sure you want to reset the bookmarked cities?")
                .setPositiveButton("Yes") { _, _ ->
                    Executors.newSingleThreadExecutor().execute(Runnable {
                        MApp.getCityDao().deleteAll()
                        Handler(Looper.getMainLooper()).post(Runnable {
                            Toast.makeText(
                                view.context,
                                "Reset bookmarks successfully",
                                Toast.LENGTH_SHORT
                            ).show()
                            (activity as MainActivity).removeAll()
                        })
                    })
                }.setNegativeButton("No") { dialog, _ ->
                    dialog.dismiss()
                }.show()
        }

        unitsRadioGroup.setOnCheckedChangeListener { group, checkedId ->
            MApp.getPref()
                .save(SystemPref.PrefKeys.units, group.findViewById<RadioButton>(checkedId).text)
        }

        forecastRadioGroup.setOnCheckedChangeListener { group, checkedId ->
            MApp.getPref()
                .save(SystemPref.PrefKeys.forecast, group.findViewById<RadioButton>(checkedId).text)
        }
    }

    override fun onResume() {
        super.onResume()

        (activity as AppCompatActivity).supportActionBar?.title = "Settings"
    }

    override fun onDestroy() {
        MApp.getPref()
            .save(SystemPref.PrefKeys.citiesLimit, cityLimitEditText.text.toString().toInt())
        super.onDestroy()
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.settings).isVisible = false;
        menu.findItem(R.id.help).isVisible = false;
        super.onPrepareOptionsMenu(menu)
    }
}