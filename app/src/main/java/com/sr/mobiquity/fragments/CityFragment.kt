package com.sr.mobiquity.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sr.mobiquity.MApp
import com.sr.mobiquity.R
import com.sr.mobiquity.adapters.ForecastAdapter
import com.sr.mobiquity.models.City
import com.sr.mobiquity.pojos.ForecastInfo
import com.sr.mobiquity.pref.SystemPref
import com.sr.pojos.WeatherInfo
import retrofit2.Call
import retrofit2.Response
import java.text.SimpleDateFormat

class CityFragment : Fragment() {
    private lateinit var city: City

    private lateinit var temperature: TextView
    private lateinit var description: TextView
    private lateinit var otherDetails: TextView
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_city, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        temperature = view.findViewById(R.id.temperatureTextView)
        description = view.findViewById(R.id.descriptionTextView)
        otherDetails = view.findViewById(R.id.oterDetailsTextView)
        recyclerView = view.findViewById(R.id.forecastRecyclerView)

        getCurrentWeatherInfo(city.latitude, city.longitude, getString(R.string.weather_api_key))
        when (MApp.getPref()[SystemPref.PrefKeys.forecast, "No"]) {
            "No" -> {
                //Do Nothing
            }

            "5 Days" -> {
                getForecastInfo(city.latitude, city.longitude, getString(R.string.weather_api_key))
            }
        }
    }

    private fun getCurrentWeatherInfo(latitude: Double, longitude: Double, apiKey: String) {
        val apiService = MApp.getAPIService()
        val call = apiService.getCurrentWeatherData(latitude, longitude, apiKey)
        call.enqueue(object : retrofit2.Callback<WeatherInfo?> {
            override fun onResponse(call: Call<WeatherInfo?>, response: Response<WeatherInfo?>) {
                response.body()?.let { updateUI(it) }
            }

            override fun onFailure(call: Call<WeatherInfo?>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun getForecastInfo(latitude: Double, longitude: Double, apiKey: String) {
        val apiService = MApp.getAPIService()
        val call = apiService.getForecastData(latitude, longitude, apiKey)
        call.enqueue(object : retrofit2.Callback<ForecastInfo?> {
            override fun onResponse(call: Call<ForecastInfo?>, response: Response<ForecastInfo?>) {
                response.body()?.let { updateForecastUI(it) }
            }

            override fun onFailure(call: Call<ForecastInfo?>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun updateForecastUI(info: ForecastInfo) {
        view?.findViewById<TextView>(R.id.city_forecast_text_view)?.visibility = View.VISIBLE
        val adapter = info.list?.let { ForecastAdapter(it) }
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = adapter
    }

    private fun updateUI(weatherInfo: WeatherInfo) {
        temperature.text = "${getString(
            R.string.set_temperature, "${weatherInfo.main?.temp}",
            MApp.getPref().getTempUnit()
        )}"
        description.text = "${weatherInfo.weather?.get(0)?.description}"
        otherDetails.text =
            "Wind: ${weatherInfo.wind?.speed} m/s\nPressure: ${weatherInfo.main?.pressure} hPa\nHumidity: ${weatherInfo.main?.humidity}%\n" +
                    "Sunrise: ${getTime(weatherInfo.sys?.sunrise!!)}\nSunset: ${getTime(weatherInfo.sys?.sunset!!)}"
    }

    private fun getTime(ms: Long): String {
        val sdf = SimpleDateFormat("hh:mm aaa")
        return sdf.format(ms)
    }

    override fun onResume() {
        super.onResume()

        (activity as AppCompatActivity).supportActionBar?.title = this.city.name
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.settings).isVisible = false;
        menu.findItem(R.id.help).isVisible = false;
        super.onPrepareOptionsMenu(menu)
    }

    companion object {
        @JvmStatic
        fun newInstance(city: City) = CityFragment()
            .apply { this.city = city }
    }
}