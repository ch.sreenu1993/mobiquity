package com.sr.mobiquity.fragments

import android.location.Address
import android.location.Geocoder
import android.media.Image
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.marginTop
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.sr.mobiquity.MApp
import com.sr.mobiquity.MainActivity
import com.sr.mobiquity.R
import com.sr.mobiquity.adapters.CitiesAdapter
import com.sr.mobiquity.models.City
import com.sr.mobiquity.pref.SystemPref
import java.io.IOException
import java.util.*
import java.util.concurrent.Executors
import kotlin.collections.ArrayList


class HomeFragment : Fragment(), OnMapReadyCallback {
    private lateinit var recyclerView: RecyclerView
    private lateinit var dragHandle: ImageView
    private var adapter: CitiesAdapter = CitiesAdapter()
    private lateinit var listener: MainActivity.ActionsListener

    private lateinit var map: GoogleMap
    private var markers = ArrayList<Marker>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.cities_recycler_view)
        dragHandle = view.findViewById(R.id.drag_handle)
        recyclerView.layoutManager = LinearLayoutManager(
            activity!!,
            LinearLayoutManager.VERTICAL,
            false
        )
        recyclerView.adapter = adapter

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        var startX = 0f
        var startY = 250f
        val margin = (dragHandle.parent as LinearLayout).marginTop
        dragHandle.setOnTouchListener(View.OnTouchListener { view, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    startX = event.x
                    startY = event.y
                }

                MotionEvent.ACTION_MOVE -> {
                    var lp =
                        (dragHandle.parent as LinearLayout).layoutParams as FrameLayout.LayoutParams
                    if (lp.topMargin <= 100 && (startY - event.y) > 0) {
                        return@OnTouchListener true
                    } else if ((dragHandle.parent as LinearLayout).height <= 150 && (startY - event.y) < 0) {
                        return@OnTouchListener true
                    }
                    lp.topMargin = lp.topMargin - (startY - event.y).toInt()
                    (dragHandle.parent as LinearLayout).layoutParams = lp
                }

                MotionEvent.ACTION_UP -> {
                    startX = event.x
                    startY = event.y
                }
            }
            return@OnTouchListener true
        })
    }

    private fun addCities() {
        Executors.newSingleThreadExecutor().execute(Runnable {
            val cities = MApp.getCityDao().getAll()
            Handler(Looper.getMainLooper()).post(Runnable {
                adapter.setListeners(listener, object :
                    AdapterListener {
                    override fun onCityDeleted(
                        city: City,
                        adapterPosition: Int
                    ) {
                        deleteMarker(city, adapterPosition)
                    }
                })
                adapter.addAll(cities)
                for (i in cities.indices) {
                    val city = cities[i]
                    val marker = map.addMarker(
                        MarkerOptions().position(LatLng(city.latitude, city.longitude))
                            .title(city.name)
                    )
                    markers.add(marker)
                }
            })
        })
    }

    private fun deleteMarker(city: City, position: Int) {
        markers.get(position).remove()
        markers.removeAt(position)
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map!!
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(17.385044, 78.486671), 10f)
        this.map.animateCamera(cameraUpdate)

        this.map.setOnMapClickListener { marker -> addMarker(marker!!) }

        addCities()
    }

    fun removeAll(){
        adapter.deleteAll();
        for (i in markers.indices){
            markers.get(i).remove()
        }
    }

    private fun addMarker(latLng: LatLng) {
        if (markers.size >= MApp.getPref().get(SystemPref.PrefKeys.citiesLimit, 10)) {
            return
        }
        Executors.newSingleThreadExecutor().execute(Runnable {
            val geoCoder = Geocoder(activity!!, Locale.getDefault())
            try {
                val addresses: List<Address> =
                    geoCoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                val address: Address = addresses[0]
                var city = City(
                    address.locality,
                    address.getAddressLine(0),
                    address.latitude,
                    address.longitude
                )

                MApp.getCityDao().insert(city)
                Handler(Looper.getMainLooper()).post(Runnable {
                    val marker =
                        map.addMarker(MarkerOptions().position(latLng).title(address.locality))!!
                    markers.add(marker)
                    adapter.addCity(city)
                })
            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.app_name)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.home_options_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.settings -> {
                listener.onSettingsSelected()
            }

            R.id.help -> {
                listener.onHelpSelected()
            }
        }
        return true
    }

    companion object {
        fun newInstance(actionsListener: MainActivity.ActionsListener) = HomeFragment()
            .apply {
                listener = actionsListener
            }
    }

    interface AdapterListener {
        fun onCityDeleted(city: City, adapterPosition: Int)
    }
}