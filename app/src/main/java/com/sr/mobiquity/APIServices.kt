package com.sr.mobiquity

import com.sr.mobiquity.pojos.ForecastInfo
import com.sr.mobiquity.pref.SystemPref
import com.sr.pojos.WeatherInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APIServices {
    @GET("weather")
    fun getCurrentWeatherData(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("appid") apiKey: String,
        @Query("units") units: String = MApp.getPref()[SystemPref.PrefKeys.units, "standard"]
    ): Call<WeatherInfo>

    @GET("forecast")
    fun getForecastData(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("appid") apiKey: String,
        @Query("units") units: String = MApp.getPref()[SystemPref.PrefKeys.units, "standard"]
    ): Call<ForecastInfo>
}