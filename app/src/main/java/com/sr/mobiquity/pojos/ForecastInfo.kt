package com.sr.mobiquity.pojos

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sr.pojos.WeatherInfo

class ForecastInfo {
    @SerializedName("cod")
    @Expose
    var cod: String? = null

    @SerializedName("message")
    @Expose
    var message: Int? = null

    @SerializedName("cnt")
    @Expose
    var cnt: Int? = null

    @SerializedName("list")
    @Expose
    var list: ArrayList<WeatherInfo>? = null

    @SerializedName("city")
    @Expose
    var city: CityInfo? = null

}