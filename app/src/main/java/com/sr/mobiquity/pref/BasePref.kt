package com.sr.mobiquity.pref

import android.content.Context
import android.content.SharedPreferences
import com.sr.mobiquity.MApp

abstract class BasePref {
    private var sharedPreferences: SharedPreferences? = null
    abstract fun init(prefName: String?): BasePref
    fun setSharedPreferences(prefName: String?) {
        sharedPreferences = MApp.getInstance().getSharedPreferences(prefName, Context.MODE_PRIVATE)
    }

    private val editor: SharedPreferences.Editor
        private get() = sharedPreferences!!.edit()

    /**
     * @param key   Key name of the storing field.
     * @param value Object value of the storing field.
     */
    fun save(key: String?, value: Any?) {
        val editor = editor
        if (value is String) {
            editor.putString(key, value as String?)
        } else if (value is Int) {
            editor.putInt(key, (value as Int?)!!)
        } else if (value is Boolean) {
            editor.putBoolean(key, (value as Boolean?)!!)
        } else if (value is Float) {
            editor.putFloat(key, (value as Float?)!!)
        } else if (value is Long) {
            editor.putLong(key, (value as Long?)!!)
        }
        editor.apply()
    }

    /**
     * @param key          Key name of the storing field.
     * @param defaultValue Default String return value if no value is stored in the required field.
     * @return Object value of the requested field.
     */
    operator fun <O> get(key: String?, defaultValue: O): O {
        val returnValue = sharedPreferences!!.all[key] as O?
        return returnValue ?: defaultValue
    }

    fun remove(key: String?) {
        if (hasKey(key)) {
            editor.remove(key).apply()
        }
    }

    fun hasKey(key: String?): Boolean {
        return sharedPreferences!!.contains(key)
    }
}