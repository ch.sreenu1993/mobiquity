package com.sr.mobiquity.pref

import com.sr.mobiquity.R


class SystemPref : BasePref() {
    override fun init(prefName: String?): SystemPref {
        setSharedPreferences(prefName)
        return this
    }

    fun getTempUnit(): String {
        when (get(PrefKeys.units, "standard")) {
            "standard" -> {
                return "K"
            }
            "metric" -> {
                return "C"
            }
            "imperial" -> {
                return "F"
            }
        }
        return "K"
    }

    internal interface PrefKeys {
        companion object {
            const val units = "units"
            const val forecast = "forecast"
            const val citiesLimit = "citiesLimit"
        }
    }

    companion object {
        const val PREF_NAME: String = com.sr.mobiquity.BuildConfig.APPLICATION_ID
    }
}